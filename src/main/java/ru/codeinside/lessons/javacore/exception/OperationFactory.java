package ru.codeinside.lessons.javacore.exception;

import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class OperationFactory {

    private final Logger log = LogManager.getLogger(OperationFactory.class);

    public static void main(String[] args) {
        var operationFactory = new OperationFactory();
        operationFactory.parseAndDivide();
        operationFactory.checkLength();
    }

    private void parseAndDivide() {
        log.info("метод parseAndDivide() успешно стартовал.");
        Stream.of("2", "5", "0", "10", "10000000000", "-100", "qwerty")
                .forEach(element -> {
                    double result = getResultParseAndDivide(element);
                    print(String.valueOf(result));
                });
        log.info("метод parseAndDivide() завершился");
    }

    private void checkLength() {
        log.info("метод checkLength() успешно стартовал.");
        Stream.of("car", "table", "", "01", "alphabet", null, "zero")
                .forEach(element -> {
                    int length = getLengthString(element);
                    print(String.valueOf(length));
                });
        log.info("метод checkLength() завершился");
    }

    private double getResultParseAndDivide(String elementArray) {
        try {
            int value = Integer.parseInt(elementArray);
            return 1000 / value;
        } catch (ArithmeticException errorException) {
            log.error("Ошибка арифметической операций: " + errorException.getMessage());
            return -1;
        } catch (NumberFormatException numberFormatException) {
            log.error("Ошибка перевода строки в число: " + numberFormatException.getMessage());
            return -1;
        }
    }

    private int getLengthString(String elementArray) {
        try {
            return elementArray.length();
        } catch (NullPointerException nullPointerException) {
            log.error("Обнаружен null");
            return -1;
        }
    }

    private void print(String msg) {
        System.out.println(msg);
    }
}
