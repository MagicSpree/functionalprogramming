package ru.codeinside.lessons.javacore.inheritance;

import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

import java.util.Collection;
import java.util.Random;

public abstract class Vehicle {

    private final String vin;
    private int yearOfProduction;
    private VehicleColor color;

    public abstract String getVehicleInfo();

    public String getVin() {
        return vin;
    }

    public Vehicle(int yearOfProduction, VehicleColor color) {
        this.yearOfProduction = yearOfProduction;
        this.color = color;
        this.vin = generateVin();
    }

    public Vehicle(int yearOfProduction, String color) {
        this.yearOfProduction = yearOfProduction;
        this.color = VehicleColor.getColorByName(color);
        this.vin = generateVin();
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public VehicleColor getColor() {
        return color;
    }

    public void setColor(VehicleColor color) {
        this.color = color;
    }

    /**
     * Метод генерирует псевдо-случайный идентификационный номер транспортного средства.
     * Данный номер используется в целях обучения и не соответствует ISO 3779-1983 и ISO 3780.
     * Переделывать или вносить изменения в этот метод не требуется.
     *
     * @return псевдо-уникальный VIN
     */
    private String generateVin() {
        Random random = new Random();
        //берем значения от 0 до Z, т.е ABCDEFGHJKLMNPQRSTUVWXYZ1234567890
        return random.ints('0', 'Z' + 1)
                .filter(i -> (i <= '9' || i >= 'A')) // Игнорируем :;<=>?@
                .limit(17) // Максимальная длина строки
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}
