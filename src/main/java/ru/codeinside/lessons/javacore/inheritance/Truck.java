package ru.codeinside.lessons.javacore.inheritance;

import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

public class Truck extends Vehicle {

    private final boolean isCanvasBack;

    Truck(int yearOfProduction, VehicleColor color, boolean isCanvasBack) {
        super(yearOfProduction, color);
        this.isCanvasBack = isCanvasBack;
    }

    Truck(int yearOfProduction, String color, boolean isCanvasBack) {
        super(yearOfProduction, color);
        this.isCanvasBack = isCanvasBack;
    }

    public boolean isCanvasBack() {
        return isCanvasBack;
    }

    @Override
    public String getVehicleInfo() {
        return String.format("Год производства %s; Цвет: %s, Натяжной тенд: %s, Vin: %s",
                getYearOfProduction(),
                getColor(),
                isCanvasBack ? "Да" : "Нет",
                getVin());
    }
}
