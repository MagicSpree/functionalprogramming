package ru.codeinside.lessons.javacore.inheritance;

import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

import java.util.Random;
import java.util.stream.Stream;

public class VehicleDemo {

    public static void main(String[] args) {
        Stream.of(
                new Truck(2020, VehicleColor.LIGHT_GREEN, true),
                new Car(2015, VehicleColor.LIGHT_GREEN, true, false)
        ).forEach(VehicleDemo::printInfoAboutVehicle);
    }

    private static void printInfoAboutVehicle(Vehicle vehicle) {
        System.out.println(vehicle.getVehicleInfo());
    }

}
