package ru.codeinside.lessons.javacore.inheritance;

import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

public class Car extends Vehicle {

    private boolean isSportCar;
    private boolean isElectroCar;

    Car(int yearOfProduction, VehicleColor color, boolean isSportCar, boolean isElectroCar) {
        super(yearOfProduction, color);
        this.isSportCar = isSportCar;
        this.isElectroCar = isElectroCar;
    }

    Car(int yearOfProduction, String color, boolean isSportCar, boolean isElectroCar) {
        super(yearOfProduction, color);
        this.isSportCar = isSportCar;
        this.isElectroCar = isElectroCar;
    }

    public boolean isSportCar() {
        return isSportCar;
    }

    public void setSportCar(boolean sportCar) {
        isSportCar = sportCar;
    }

    public boolean isElectroCar() {
        return isElectroCar;
    }

    public void setElectroCar(boolean electroCar) {
        isElectroCar = electroCar;
    }

    @Override
    public String getVehicleInfo() {
        return String.format("Год производства %s; Цвет: %s, Машина спортивная: %s, Машина электрическая: %s, Vin: %s",
                getYearOfProduction(),
                getColor(),
                isSportCar ? "Да" : "Нет",
                isElectroCar ? "Да" : "Нет",
                getVin());
    }

}
